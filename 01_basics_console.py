import tensorflow as tf

# Start session
sess = tf.InteractiveSession()

# Create input
a_input = tf.placeholder(tf.int32, name='a_input')

# Create variables
b_var = tf.Variable(4, name='b_var')

# Create operation tensor
times_4 = tf.multiply(a_input, b_var)

print(a_input)
print(b_var)
print(times_4)

# Initialise the variables
sess.run(tf.global_variables_initializer())

# Evaluate the tensor
result = sess.run(times_4, feed_dict={a_input: 3})
print(result)