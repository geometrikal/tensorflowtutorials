import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler

# Read data
morph = pd.read_csv("morphology.csv")

# Convert labels to categories
labels = pd.Categorical(morph.label)
morph.head()

# Select features
vectors = morph.loc[:, "area":"6thmoment"]
scaler = MinMaxScaler()
vectors = scaler.fit_transform(vectors.values)

# Create training set
x_train, x_test, y_train, y_test = train_test_split(vectors, labels.codes, test_size=0.25, random_state=42)

# Select optimizer
opt = tf.keras.optimizers.Nadam()

# Create Network
model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Dense(120, input_dim=vectors.shape[1], activation='relu'))
model.add(tf.keras.layers.Dense(80, activation='relu'))
model.add(tf.keras.layers.Dropout(0.3,seed=7))
model.add(tf.keras.layers.Dense(len(labels.categories), activation='softmax'))
model.compile(loss='sparse_categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
model.summary()

# Fit network
model.fit(x_train, y_train, epochs=100, verbose=1, batch_size=100, validation_data=(x_test, y_test))

# Evaluate
model.evaluate(x_test, y_test)

# Example prediction
print(model.predict_classes(x_test[0:10]))
print(np.max(model.predict(x_test[0:10]), axis=1))
print(y_test[0:10])