import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

# Create noisy signal
x = np.arange(0,100, dtype=np.float32)
y = x * 0.3 + np.random.randn(100) + 10

# Plot
plt.plot(x,y,'.')
plt.show()

# Start session
sess = tf.InteractiveSession()

# Create inputs
x_input = tf.placeholder(tf.float32, [None,1])
y_input = tf.placeholder(tf.float32, [None,1])

# Create variables
m_var = tf.Variable(tf.zeros([1,1]))
c_var = tf.Variable(tf.zeros([1]))

# Create operation
y_output = tf.add(tf.matmul(x_input,m_var),c_var)


# LOSS
# Optimisation function - mean square loss
loss = tf.reduce_mean(tf.square(y_output - y_input))




# TRAINING
learning_rate = 0.0001
training_epochs = 10000

# Use gradient descent optimiser
optimiser = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

# Initialise variables
sess.run(tf.global_variables_initializer())

# Loss before training
result = sess.run(loss, feed_dict = {x_input: x[:,np.newaxis], y_input: y[:,np.newaxis]})
print("Loss total before training: %f" % result)

# Train in batches
# - randomise the order of measurements
# - split total number of measurements into 10 batches
# - run loss operation for each batch, this will update the variables
# - repeat until certain number of epochs (full sets of measurements) have been fed to the network
split_count = 10
for epoch in range(training_epochs):
    idx = np.arange(0,len(x),1)
    idx = np.random.permutation(idx)
    splits = np.split(idx, split_count)
    
    for split in splits:
        feed_dict = {x_input: x[split,np.newaxis], y_input: y[split,np.newaxis]}
        sess.run(optimiser,feed_dict=feed_dict)
        
    if epoch % 500 == 0:
        feed_dict = {x_input: x[:,np.newaxis], y_input: y[:,np.newaxis]}
        current_loss = sess.run(loss,feed_dict=feed_dict)
        print("Epoch: {}, loss: {:0.2f}, ".format(epoch, current_loss))