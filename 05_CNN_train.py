import numpy as np
import glob
import os
from PIL import Image
from skimage.transform import resize
from skimage.exposure import rescale_intensity
from sklearn.model_selection import train_test_split
import tensorflow as tf


def process_image(filename, width):
    im = Image.open(filename).convert('L')  # open and convert to greyscale
    im = np.asarray(im, dtype=np.float)  # numpy array
    im_shape = im.shape
    im = resize(im, [width, width], order=1)  # resize using linear interpolation
    im = np.divide(im, 255)  # divide to put in range [0 - 1]
    return im, im_shape


def load_from_class_dirs(directory, extension, width, norm, min_count=20):
    print("# INIT #   Load dataset from class dirs, " + directory)

    # Init lists
    images = []
    labels = []
    cls = []
    filenames = []

    # Alphabetically sorted classes
    class_dirs = sorted(glob.glob(directory + "/*"))

    # Load images from each class
    idx = 0
    for class_dir in class_dirs:

        # Class name
        class_name = os.path.basename(class_dir)
        num_files = len(os.listdir(class_dir))
        print("%s - %d" % (class_name, num_files))
        if num_files < min_count:
            continue

        class_idx = idx
        idx += 1
        labels.append(class_name)

        # Get the files
        files = sorted(glob.glob(class_dir + "/*." + extension))
        for file in files:
            im, sz = process_image(file, width)
            if norm:
                im = rescale_intensity(im, in_range='image', out_range=(0.0, 1.0))
            images.append(im)
            cls.append(class_idx)
            filenames.append(os.path.basename(os.path.dirname(file)) + os.path.sep + os.path.basename(file))

    # Final clean up
    images = np.asarray(images)
    cls = np.asarray(cls)
    num_classes = len(labels)

    return images, cls, labels, num_classes, filenames


images, cls, labels, num_classes, filenames = load_from_class_dirs("images", "png", 64, False, min_count=0)
images = images[:,:,:,np.newaxis]

x_train, x_test, y_train, y_test = train_test_split(images, cls, test_size=0.25, random_state=42)

# Optimizer
opt = tf.keras.optimizers.Adam()

# Network
model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Conv2D(16, (5,5), input_shape=(64, 64, 1), activation='relu', padding='same'))
model.add(tf.keras.layers.MaxPooling2D())
model.add(tf.keras.layers.Conv2D(32, (5,5), activation='relu', padding='same'))
model.add(tf.keras.layers.MaxPooling2D())
model.add(tf.keras.layers.Conv2D(64, (5,5), activation='relu', padding='same'))
model.add(tf.keras.layers.MaxPooling2D())
model.add(tf.keras.layers.Conv2D(128, (5,5), activation='relu', padding='same'))
model.add(tf.keras.layers.MaxPooling2D())
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dropout(0.5,seed=7))
model.add(tf.keras.layers.Dense(512, activation='relu'))
model.add(tf.keras.layers.Dense(len(labels), activation='softmax'))
model.count_params()

model.compile(loss='sparse_categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
model.summary()

model.fit(x_train, y_train, epochs=200, verbose=1, batch_size=100, validation_data=(x_test, y_test))

model.evaluate(x_test, y_test)

print(model.predict_classes(x_test[0:10]))
print(np.max(model.predict(x_test[0:10]), axis=1))
print(y_test[0:10])
